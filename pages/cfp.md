---
name: Call for Proposals
---

# Call for Proposals

<!--
To: debian-devel-announce@lists.debian.org, debconf-announce@lists.debian.org
Subject: Call for Proposals: DebConf23, India
-->

The Call for Proposals has not been opened, yet.

<!--
The DebConf Content team would like to call for proposals for the DebConf23
conference, which will take place in Kochi, India, from September 10th to 17th, 2023.
It will be preceded by DebCamp from September 3rd to 10th.

## Submitting an Event

You can now submit an [event proposal]. Events are not limited to
traditional presentations or informal sessions (BoFs): we welcome
submissions of tutorials, performances, art installations, debates, or any
other format of event that you think would be of interest to the Debian
community.

Regular sessions may either be 20 or 45 minutes long (including time for
questions), other kinds of sessions (workshops, demos, lightning talks,
...) could have different durations. Please choose the most suitable
duration for your event and explain any special requests.

In order to submit a talk, you will need to create an account on the site.
We suggest that Debian Salsa account holders (including DDs and DMs) use
their [Salsa] login when creating an account. However, this isn't required,
as you can sign up with an e-mail address and password.

[event proposal]: https://debconf23.debconf.org/talks/new/
[Salsa]: https://salsa.debian.org/

## Timeline

* Submission deadline: Sunday, May 1st.

After the deadline, we are still accepting event proposals for ad-hoc sessions
(self-organized sessions).

## Topics and Tracks

Though we invite proposals on any Debian or FLOSS related subject, we have
some broad topics on which we encourage people to submit proposals,
including but not limited to:

- Introduction to Free Software & Debian
- Packaging, policy, and Debian infrastructure
- Systems administration, automation and orchestration
- Cloud and containers
- Security
- Community, diversity, local outreach and social context
- Internationalization, Localization and Accessibility
- Embedded & Kernel
- Debian in Arts & Science
- Debian Blends and Debian derived distributions

## Talk proposal help on IRC

If you want to submit a talk proposal but would like to discuss it first, feel free to
pop by the #debconf-content IRC channel on OFTC, where the DebConf content team will be
available to help potential speakers prepare their talk proposals for DebConf.

Just make sure you stick around the channel if you don't get an immediate response,
 because it could be coming from the other side of the world.

## Code of Conduct

Our event is covered by a [Code of Conduct] designed to ensure everyone’s
safety and comfort. The code applies to all attendees, including speakers
and the content of their presentations. Do not hesitate to contact us at
<content@debconf.org> if you have any questions or are unsure about certain
content you’d like to present.

[Code of Conduct]: https://debconf.org/codeofconduct.shtml


## Video Coverage

Providing video is one of the [conference goals], as it makes the content
accessible to a wider audience. Unless speakers opt-out, scheduled talks may be
streamed live over the Internet to promote remote participation, and recordings
will be published later under the [DebConf license] (MIT/Expat), as well as
presentation slides and papers whenever available.

[conference goals]: https://debconf.org/goals.shtml
[DebConf license]:  https://meetings-archive.debian.net/pub/debian-meetings/LICENSE

## Closing note

DebConf23 is accepting sponsors; if you are interested, or think you know
of others who would be willing to help, please get in touch with
<sponsors@debconf.org>.

In case of any questions, or if you wanted to bounce some ideas off us
first, please do not hesitate to reach out to the content team at
<content@debconf.org>.

We hope to see you in Kochi!

The DebConf team
-->
