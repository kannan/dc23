from django import template


register = template.Library()


@register.filter
def stream(venue):
    raise NotImplementedError(f"No video stream for {venue.name}")


@register.filter
def channel(venue):
    raise NotImplementedError(f"No IRC channel for {venue.name}")
